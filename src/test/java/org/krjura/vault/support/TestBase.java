package org.krjura.vault.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.krjura.vault.repository.AllRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@ExtendWith(SpringExtension.class)
@ProjectTest
public class TestBase {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private AllRepositories allRepositories;

    @LocalServerPort
    private int port;

    private MockMvc mvc;

    private HttpClient httpClient;

    private ObjectMapper objectMapper;

    @BeforeEach
    public void beforeTestBase()  {
        buildMvc();
        buildObjectMapper();
        buildHttpClient();
        cleanDb();
    }

    private void buildMvc() {
        this.mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    private void buildObjectMapper() {
        this.objectMapper = new ObjectMapper();
    }

    private void buildHttpClient() {
        httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.NEVER)
                .connectTimeout(Duration.ofSeconds(2))
                .build();
    }

    private void cleanDb() {
        this.allRepositories.appUser().deleteAll();
    }

    public MockMvc getMvc() {
        return mvc;
    }

    public int getPort() {
        return port;
    }

    public HttpClient getHttpClient() {
        return httpClient;
    }

    public String localUrl(String resource) {
        return "http://127.0.0.1:" + getPort() + resource;
    }

    public byte[] contentOf(String fileName) throws URISyntaxException, IOException {
        return Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource(fileName).toURI())
        );
    }

    public String stringContentOf(String fileName) throws URISyntaxException, IOException {
        return new String(contentOf(fileName), StandardCharsets.UTF_8);
    }

    public <T> T fromJson(String content, Class<T> clazz) throws IOException {
        return this.objectMapper.readValue(content, clazz);
    }

    public String toJson(Object data) throws IOException {
        return this.objectMapper.writeValueAsString(data);
    }
}