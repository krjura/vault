package org.krjura.vault.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("app_user")
public final class AppUser {

    @Id
    private final Long id;

    @Column("email")
    private final String email;

    @Column("password")
    private final String password;

    @PersistenceConstructor
    public AppUser(Long id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public AppUser withId(Long id) {
        return new AppUser(id, this.email, this.password);
    }

    public Long id() {
        return id;
    }

    public String email() {
        return email;
    }

    public String password() {
        return password;
    }
}
