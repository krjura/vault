#!/usr/bin/env bash

export PGPASSWORD=password
export ARGS="-h 127.0.0.1 -U postgres"

psql $ARGS -c "DROP DATABASE IF EXISTS vault_db;"
psql $ARGS -c "CREATE DATABASE vault_db ENCODING 'UTF-8'"

psql $ARGS -c "DROP USER IF EXISTS vault_db_user;"
psql $ARGS -c "CREATE USER vault_db_user WITH PASSWORD 'vault_db_user';"