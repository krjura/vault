package org.krjura.vault.support;

import org.springframework.test.context.ActiveProfilesResolver;

public class ProjectActiveProfilesResolver implements ActiveProfilesResolver {

    private static final String SPRING_PROFILES_ACTIVE = "spring.profiles.active";
    private static final String PROFILE_SEPARATOR = ",";

    @Override
    public String[] resolve(Class<?> testClass) {
        String activeProfiles = System.getProperty(SPRING_PROFILES_ACTIVE);

        if (activeProfiles == null || activeProfiles.isEmpty()) {
            return new String[]{"test"};
        }

        return activeProfiles.split(PROFILE_SEPARATOR);
    }
}
