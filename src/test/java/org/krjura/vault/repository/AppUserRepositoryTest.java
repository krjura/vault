package org.krjura.vault.repository;

import org.junit.jupiter.api.Test;
import org.krjura.vault.model.AppUser;
import org.krjura.vault.support.TestBase;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

class AppUserRepositoryTest extends TestBase {

    @Autowired
    private AppUserRepository appUserRepository;

    @Test
    public void just_make_sure_we_can_read_and_write() {
        var entity = new AppUser(
                null, "tester@example.com", "$2a$10$nB1ZwMzvu6FLyzXioEbcIuIG16uf/5DxIZRU9vnGCBgQYBibSMT0a");

        entity = appUserRepository.save(entity);

        var byId = appUserRepository.findById(entity.id());
        assertThat(byId).isNotNull();
        assertThat(byId).isPresent();
        assertThat(byId.get().id()).isEqualTo(entity.id());
        assertThat(byId.get().email()).isEqualTo(entity.email());
        assertThat(byId.get().password()).isEqualTo(entity.password());

        var byEmail = appUserRepository.findByEmail(entity.email());
        assertThat(byEmail).isNotNull();
        assertThat(byEmail).isPresent();
        assertThat(byEmail.get().id()).isEqualTo(entity.id());
        assertThat(byEmail.get().email()).isEqualTo(entity.email());
        assertThat(byEmail.get().password()).isEqualTo(entity.password());
    }

    @Test
    public void just_make_sure_we_can_delete_by_id() {
        var entity = new AppUser(
                null, "tester@example.com", "$2a$10$nB1ZwMzvu6FLyzXioEbcIuIG16uf/5DxIZRU9vnGCBgQYBibSMT0a");

        entity = appUserRepository.save(entity);

        var byId = appUserRepository.findById(entity.id());
        assertThat(byId).isNotNull();
        assertThat(byId).isPresent();

        appUserRepository.deleteById(byId.get().id());

        var byIdAfterDelete = appUserRepository.findById(entity.id());
        assertThat(byIdAfterDelete).isNotNull();
        assertThat(byIdAfterDelete).isEmpty();
    }

    @Test
    public void just_make_sure_we_can_delete_by_email() {
        var entity = new AppUser(
                null, "tester@example.com", "$2a$10$nB1ZwMzvu6FLyzXioEbcIuIG16uf/5DxIZRU9vnGCBgQYBibSMT0a");

        entity = appUserRepository.save(entity);

        var byEmail = appUserRepository.findByEmail(entity.email());
        assertThat(byEmail).isNotNull();
        assertThat(byEmail).isPresent();

        appUserRepository.deleteByEmail(entity.email());

        var byEmailAfterDelete = appUserRepository.findByEmail(entity.email());
        assertThat(byEmailAfterDelete).isNotNull();
        assertThat(byEmailAfterDelete).isEmpty();
    }

}