#!/usr/bin/env bash

export PGPASSWORD=password
export ARGS="-h 127.0.0.1 -U postgres"

psql $ARGS -d vault_db
