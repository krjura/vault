package org.krjura.vault.repository;

import org.springframework.stereotype.Component;

@Component
public class AllRepositories {

    private final AppUserRepository appUserRepository;

    public AllRepositories(AppUserRepository appUserRepository) {
        this.appUserRepository = appUserRepository;
    }

    public AppUserRepository appUser() {
        return appUserRepository;
    }
}
