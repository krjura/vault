package org.krjura.vault.repository;

import java.util.Optional;
import org.krjura.vault.model.AppUser;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppUserRepository extends CrudRepository<AppUser, Long> {

    @Query("SELECT * from app_user where email = :email")
    Optional<AppUser> findByEmail(String email);

    @Modifying
    @Query("DELETE FROM app_user where email = :email")
    int deleteByEmail(String email);

}
